<?php

// Plugin definition
$plugin = array(
  'title' => t('Zen Panels Layout'),
  'category' => t('Columns: 3'),
  'icon' => 'zen_layout.png',
  'theme' => 'zen_layout',
  'theme arguments' => array('id', 'content'),
  'regions' => array(
    'left' => t('Left side'),
    'middle' => t('Middle column'),
    'right' => t('Right side')
  ),
);

if (arg(0) == 'admin' && arg(1) == 'structure') {
  $plugin['theme'] = 'panels_threecol_25_50_25';
  $plugin['css'] = 'threecol_25_50_25.css';
}

